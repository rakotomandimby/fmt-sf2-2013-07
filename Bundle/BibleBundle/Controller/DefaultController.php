<?php

namespace Netapsys\Bundle\BibleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Netapsys\Bundle\BibleBundle\Entity\Kjv;
use Netapsys\Bundle\BibleBundle\Entity\Commentaire;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;



class DefaultController extends Controller
{
    /**
     * @Template()
     */
    public function indexAction($name, $chapter, $vmin)
    {

        $em = $this->getDoctrine()->getManager();
        
        $verse_repository=$this->getDoctrine()
                         ->getRepository('NetapsysBibleBundle:Kjv');
        $commentaire_repository=$this->getDoctrine()
                         ->getRepository('NetapsysBibleBundle:Commentaire');

        $verse=$verse_repository->findOneBy(array('bname' => $name, 
                                                  'cnum'  => $chapter,
                                                  'vnum'  => $vmin));        
        $reference_verse=$verse->getBnum().','.$verse->getCnum().','.$verse->getVnum();


        $liste_commentaires=$commentaire_repository->findByVerset($reference_verse);

        $commentaire = new Commentaire();
        $commentaire->setVerset($reference_verse);

        $form = $this->createFormBuilder($commentaire)
                     ->add('texte', 'textarea')
                     ->add('verset', 'hidden')
                     ->getForm();
        // Attention:
        // Dans la doc PDF c est $this->createBuilder(...)
        // Qui n'existe plus!

        $request=$this->getRequest();
        
        if ($request->isMethod('POST')) 
            {
                $form->bind($request);
                if ($form->isValid()) 
                    {
                        // Les data sont aussi dans $request->request->get('form')['texte']
                        // Mais en plus propre (subjectif):
                        // $form->getData() est un objet Commentaire
                        $commentaire->setTexte($form->getData()->getTexte());
                        $em->persist($commentaire);
                        $em->flush();
                        return $this->redirect($this->generateUrl('netapsys_bible_single_verse',
                                                                  array('name'    => $name, 
                                                                        'chapter' => $chapter,
                                                                        'vmin'=> $vmin)));
                    }
            }
        return array(
            'name'    => $name,
            'chapter' => $chapter,
            'vmin'    => $vmin,
            'vtext'   => $verse->getVtext(),
            'commentaires' => $liste_commentaires,
            'form'    => $form->createView());
    }


    /**
     * @Template()
     */
    public function chapterAction($name, $chapter)
    {
        $repository=$this->getDoctrine()->getRepository('NetapsysBibleBundle:Kjv');
        $verses=$repository->findBy(array('bname' => $name, 'cnum' => $chapter));        
        return array('verses' => $verses);
    }


    /**
     * @Template()
     */
    public function rangeAction($name, $chapter, $vmin, $vmax)
    {

        $em = $this->getDoctrine()
                   ->getEntityManager();
        // ->getRepository('NetapsysBibleBundle:Kjv');
        // $verses=$em->verseRange($name, $chapter, $vmin, $vmax);
        $query = $em->createQuery('SELECT v
                                   FROM  NetapsysBibleBundle:Kjv v 
                                   WHERE v.bname = :name 
                                     AND v.cnum = :chapter 
                                     AND (v.vnum <= :vmax AND v.vnum >= :vmin) ')
                    ->setParameters(array('name'    => $name,
                                          'chapter' => $chapter, 
                                          'vmax'    => $vmax, 
                                          'vmin'    => $vmin));
        $verses = $query->getResult();


        return array('verses' => $verses);
    }
}
