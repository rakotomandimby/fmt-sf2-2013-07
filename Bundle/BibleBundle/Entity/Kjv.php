<?php

namespace Netapsys\Bundle\BibleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Kjv
 *
 * @ORM\Table(name="kjv")
 * @ORM\Entity(repositoryClass="Netapsys\Bundle\BibleBundle\Repository\KjvRepository")
 */
class Kjv
{
    /**
     * @var string
     *
     * @ORM\Column(name="bsect", type="string", nullable=false)
     */
    private $bsect;

    /**
     * @var integer
     *
     * @ORM\Column(name="bnum", type="integer", nullable=false)
     */
    private $bnum;

    /**
     * @var string
     *
     * @ORM\Column(name="vtext", type="text", nullable=false)
     */
    private $vtext;

    /**
     * @var string
     *
     * @ORM\Column(name="bname", type="string", length=20)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $bname;

    /**
     * @var integer
     *
     * @ORM\Column(name="cnum", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $cnum;

    /**
     * @var integer
     *
     * @ORM\Column(name="vnum", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $vnum;



    /**
     * Set bsect
     *
     * @param string $bsect
     * @return Kjv
     */
    public function setBsect($bsect)
    {
        $this->bsect = $bsect;
    
        return $this;
    }

    /**
     * Get bsect
     *
     * @return string 
     */
    public function getBsect()
    {
        return $this->bsect;
    }

    /**
     * Set bnum
     *
     * @param integer $bnum
     * @return Kjv
     */
    public function setBnum($bnum)
    {
        $this->bnum = $bnum;
    
        return $this;
    }

    /**
     * Get bnum
     *
     * @return integer 
     */
    public function getBnum()
    {
        return $this->bnum;
    }

    /**
     * Set vtext
     *
     * @param string $vtext
     * @return Kjv
     */
    public function setVtext($vtext)
    {
        $this->vtext = $vtext;
    
        return $this;
    }

    /**
     * Get vtext
     *
     * @return string 
     */
    public function getVtext()
    {
        return $this->vtext;
    }

    /**
     * Set bname
     *
     * @param string $bname
     * @return Kjv
     */
    public function setBname($bname)
    {
        $this->bname = $bname;
    
        return $this;
    }

    /**
     * Get bname
     *
     * @return string 
     */
    public function getBname()
    {
        return $this->bname;
    }

    /**
     * Set cnum
     *
     * @param integer $cnum
     * @return Kjv
     */
    public function setCnum($cnum)
    {
        $this->cnum = $cnum;
    
        return $this;
    }

    /**
     * Get cnum
     *
     * @return integer 
     */
    public function getCnum()
    {
        return $this->cnum;
    }

    /**
     * Set vnum
     *
     * @param integer $vnum
     * @return Kjv
     */
    public function setVnum($vnum)
    {
        $this->vnum = $vnum;
    
        return $this;
    }

    /**
     * Get vnum
     *
     * @return integer 
     */
    public function getVnum()
    {
        return $this->vnum;
    }
}