<?php

namespace Netapsys\Bundle\BibleBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Commentaire
 *
 * @ORM\Table(name="commentaire")
 * @ORM\Entity(repositoryClass="Netapsys\Bundle\BibleBundle\Entity\CommentaireRepository")
 */
class Commentaire
{
    /**
     * @var string
     *
     * @ORM\Column(name="verset", type="string", length=16, nullable=true)
     */
    private $verset;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(max = 10)
     * @ORM\Column(name="texte", type="text", nullable=true)
     */
    private $texte;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    /**
     * Set verset
     *
     * @param string $verset
     * @return Commentaire
     */
    public function setVerset($verset)
    {
        $this->verset = $verset;
    
        return $this;
    }

    /**
     * Get verset
     *
     * @return string 
     */
    public function getVerset()
    {
        return $this->verset;
    }

    /**
     * Set texte
     *
     * @param string $texte
     * @return Commentaire
     */
    public function setTexte($texte)
    {
        $this->texte = $texte;
    
        return $this;
    }

    /**
     * Get texte
     *
     * @return string 
     */
    public function getTexte()
    {
        return $this->texte;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}