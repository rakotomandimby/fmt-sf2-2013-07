<?php

namespace Netapsys\Bundle\ConverterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($temperature, $initialunit)
    {
        if($initialunit == "fahrenheit")
            {
                $result=$temperature*9/5+32;
                $resultunit="celcius";
            }
        elseif($initialunit == "celcius")
            {
                $result=(5/9)*($temperature-32);
                $resultunit="fahrenheit";
            }
        return $this->render('NetapsysConverterBundle:Default:index.html.twig', 
                             array('temperature' => $temperature,
                                   'initialunit' => $initialunit,
                                   'result'=> $result,
                                   'resultunit' => $resultunit));
    }
}
